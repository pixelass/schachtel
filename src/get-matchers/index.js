import matchMedia from '../match-media'

/**
 * get a collection of all breakpoint matchers
 * @param  {Number} baseSize base column size
 * @param  {Number} handheldCols number of columns for the handheld breakpoint
 * @param  {Number} tabletCols   number of columns for the tablet breakpoint
 * @param  {Number} desktopCols  number of columns for the desktop breakpoint
 * @return {Object} object containing the mediaMathcers for each breakpoint
 */
const getMatchers = (baseSize, handheldCols, tabletCols, desktopCols) => (
  {
    handheld: matchMedia({
      maxWidth: `${baseSize * (handheldCols + 2)}px`
    }),
    tablet: matchMedia({
      minWidth: `${baseSize * (handheldCols + 2) + 1}px`,
      maxWidth: `${baseSize * (tabletCols + 2)}px`
    }),
    desktop: matchMedia({
      minWidth: `${baseSize * (tabletCols + 2) + 1}px`,
      maxWidth: `${baseSize * (desktopCols + 2)}px`
    }),
    full: matchMedia({
      minWidth: `${baseSize * (desktopCols + 2) + 1}px`
    })
  }
)

export default getMatchers
