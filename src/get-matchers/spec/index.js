/* eslint-env jest */
import '../../match-media-mock'
import getMatchers from '..'

describe('getMatchers', () => {
  it('returns an object', () => {
    const matchers = getMatchers(80, 4, 8, 12)
    expect(typeof matchers).toBe('object')
  })
  it('returns a handheld matcher', () => {
    const matchers = getMatchers(80, 4, 8, 12)
    expect(typeof matchers.handheld).toBe('object')
  })
  it('returns a tablet matcher', () => {
    const matchers = getMatchers(80, 4, 8, 12)
    expect(typeof matchers.tablet).toBe('object')
  })
  it('returns a desktop matcher', () => {
    const matchers = getMatchers(80, 4, 8, 12)
    expect(typeof matchers.desktop).toBe('object')
  })
  it('returns a full matcher', () => {
    const matchers = getMatchers(80, 4, 8, 12)
    expect(typeof matchers.full).toBe('object')
  })
})
