import React, {Component, PropTypes} from 'react'
import getMatchers from '../get-matchers'

/**
 * check an array for the first match. Include `0` as a
 * valid return value.
 * @param  {Array.<Number>} arr Array of `Numbers` to check
 * @return {Number} returns the first match
 */
const checkValues = arr => {
  let ret
  arr.forEach(value => {
    ret = ret === 0 ? ret : ret || value
  })
  return ret
}

/**
 * a column that relies on context. When using a `<Column/>` it
 * defaults to 1 grid unit. The base size is defined via context
 * and defaults to `80px`.
 *
 * while columns can be nested directly inside another, extra padding
 * would be added. To remove the gutters a `SubGrid` is required.
 *
 * This is a mobile-first grid, therefore the first property to set
 * would be `handheld`, which defaults to `1`. The next step `desktop`
 * also defines all larger sizes unless changed.
 * @example
 * <Column>1</Column>
 * <Column handheld={2}>2</Column>
 * <Column handheld={2}
 *         tablet={5}>
 *   2 -> 5
 * </Column>
 * <Column handheld={2}
 *         tablet={5}
 *         desktop={7}>
 *   2 -> 5 -> 7
 * </Column>
 * <Column handheld={2}
 *         tablet={5}
 *         desktop={7}
 *         full={4}>
 *   2 -> 5 -> 7 -> 4
 * </Column>
 * <Column handheld={12}
 *         full={4}>
 *   4 -> 8 -> 12 -> 4
 * </Column>
 */
class Column extends Component {
  /**
   * constructor binds methods and allows state
   * @param  {Object} [props] options to use for the column instannce.
   * @param  {Number} [props.handheld=1] span units on handheld size.
   * @param  {Number} [props.tablet] span units on tablet size.
   * @param  {Number} [props.desktop] span units on desktop size.
   * @param  {Number} [props.full] span units on anythin larger than desktop.
   * @param  {String} [props.el='div'] element to render as column.
   * @param  {String} [props.className] additional classNames to add to the column.
   * @param  {String} [props.children] children to render (usually the nested content).
   * @return {ReactElement} returns a DOMnode with the column logic
   */
  constructor (props) {
    super(props)
    this.handleHandheld = this.handleHandheld.bind(this)
    this.handleTablet = this.handleTablet.bind(this)
    this.handleDesktop = this.handleDesktop.bind(this)
    this.handleFull = this.handleFull.bind(this)
    this.state = {
      size: 0
    }
  }

  getChildContext () {
    return {
      columns: this.state.size
    }
  }

  /**
   * before the component mounts we need to add media listeners.
   * each breakpoint exactly matches a certain width. There are no overlaps
   * of breakpoints.
   *
   * When the listeners are created we need to call them once to get the initial layout.
   * This should happen before we render the component.
   * @private
   */
  componentWillMount () {
    const {baseSize, handheldCols, tabletCols, desktopCols} = this.context
    const matchers = getMatchers(baseSize, handheldCols, tabletCols, desktopCols)

    this.handheld = matchers.handheld
    this.tablet = matchers.tablet
    this.desktop = matchers.desktop
    this.full = matchers.full

    this.handheld.addListener(this.handleHandheld)
    this.tablet.addListener(this.handleTablet)
    this.desktop.addListener(this.handleDesktop)
    this.full.addListener(this.handleFull)

    this.handleHandheld(this.handheld)
    this.handleTablet(this.tablet)
    this.handleDesktop(this.desktop)
    this.handleFull(this.full)
  }

  /**
   * before the component is unmounted we need to make sure all
   * listeners are removed properly.
   * @private
   */
  componentWillUnmount () {
    this.handheld.removeListener(this.handleHandheld)
    this.tablet.removeListener(this.handleTablet)
    this.desktop.removeListener(this.handleDesktop)
    this.full.removeListener(this.handleFull)
  }

  /**
   * handle the handheld breakpoint
   * @private
   * @param  {Object} mql mediaQueryListener for handheld
   */
  handleHandheld (mql) {
    if (mql.matches) {
      const size = Math.min(this.context.columns, this.props.handheld)
      this.setState({
        size
      })
    }
  }

  /**
   * handle the tablet breakpoint
   * @private
   * @param  {Object} mql mediaQueryListener for tablet
   */
  handleTablet (mql) {
    if (mql.matches) {
      const value = checkValues([
        this.props.tablet,
        this.props.handheld
      ])
      const size = Math.min(this.context.columns, value)
      this.setState({
        size
      })
    }
  }

  /**
   * handle the desktop breakpoint
   * @private
   * @param  {Object} mql mediaQueryListener for desktop
   */
  handleDesktop (mql) {
    if (mql.matches) {
      const value = checkValues([
        this.props.desktop,
        this.props.tablet,
        this.props.handheld
      ])
      const size = Math.min(this.context.columns, value)
      this.setState({
        size
      })
    }
  }

  /**
   * handle the full-width breakpoint
   * @private
   * @param  {Object} mql mediaQueryListener for full-width
   */
  handleFull (mql) {
    if (mql.matches) {
      const value = checkValues([
        this.props.full,
        this.props.desktop,
        this.props.tablet,
        this.props.handheld
      ])
      const size = Math.min(this.context.columns, value)
      this.setState({
        size
      })
    }
  }

  render () {
    if (this.state.size === 0) {
      return null
    }
    const style = {
      width: `${100 / this.context.columns * this.state.size}%`,
      padding: `0 ${this.context.gutter}px`,
      boxSizing: 'border-box'
    }
    return (
      <this.props.el style={style} className={this.props.className}>
        {this.props.children}
      </this.props.el>
    )
  }
}

Column.contextTypes = {
  columns: PropTypes.number,
  gutter: PropTypes.number,
  handheldCols: PropTypes.number,
  tabletCols: PropTypes.number,
  desktopCols: PropTypes.number,
  fullCols: PropTypes.number,
  baseSize: PropTypes.number
}

Column.childContextTypes = {
  columns: PropTypes.number
}

Column.defaultProps = {
  el: 'div',
  handheld: 1
}

Column.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  el: PropTypes.string,
  handheld: PropTypes.number,
  tablet: PropTypes.number,
  desktop: PropTypes.number,
  full: PropTypes.number
}

export default Column
