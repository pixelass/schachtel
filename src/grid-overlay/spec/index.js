/* eslint-env jest */
import React from 'react'
import {mount} from 'enzyme'
import '../../match-media-mock'

import GridOverlay from '..'
import Column from '../../column'

describe('<GridOverlay />', () => {
  it('renders twelve <Column /> components', () => {
    const wrapper = mount(<GridOverlay visible/>)
    expect(wrapper.find(Column).length).toBe(12)
  })
  it('allows us to change the visibility', () => {
    const wrapper = mount(<GridOverlay/>)
    expect(wrapper.props().visible).toBeFalsy()
    wrapper.setProps({ visible: true })
    expect(wrapper.props().visible).toBeTruthy()
  })
  it('allows us to hide it', () => {
    const wrapper = mount(<GridOverlay/>)
    expect(wrapper.props().visible).toBeFalsy()
    expect(wrapper.html()).toEqual(null)
  })
  it('allows us to show it', () => {
    const wrapper = mount(<GridOverlay visible/>)
    expect(wrapper.props().visible).toBeTruthy()
    expect(wrapper.html()).not.toEqual(null)
  })
})
