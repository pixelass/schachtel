import React, {Component, PropTypes} from 'react'
import Grid from '../grid'
import Column from '../column'
import styles from './style.css'

/**
 * The GridOverlay allows to show a debugging layer
 */
class GridOverlay extends Component {
  /**
   * [constructor description]
   * @param  {Object} [props] options
   * @param  {Object} [props.visible=false] show only if true
   * @return {ReactElement | null} renders a grid layer or `null`
   */
  constructor (props) { // eslint-disable-line no-useless-constructor
    super(props)
  }

  render () {
    if (!this.props.visible) {
      return null
    }
    const columns = []
    let counter = 12
    while (counter--) {
      columns.push(<Column key={counter} className={styles.schachtelDebug}/>)
    }
    return (
      <Grid className={styles.schachtelOverlay}>
        {columns}
      </Grid>
    )
  }
}

GridOverlay.defaultProps = {
  visible: false
}

GridOverlay.propTypes = {
  visible: PropTypes.bool
}

export default GridOverlay
