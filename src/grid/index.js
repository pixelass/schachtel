import React, {Component, PropTypes} from 'react'
import classNames from 'classnames'
import getMatchers from '../get-matchers'

import styles from './style.css'

/**
 * A mobile first grid that works with context.
 * Each `Grid` creates a context based grid that provides
 * a very powerful layout tool.
 * @example
 * <Grid>
 *   <Column/>
 * </Grid>
 * <Grid gutter={6}>
 *   <Column/>
 * </Grid>
 */
class Grid extends Component {
  /**
   * Grid instance
   * @param  {Object} [props] options to configure the grid
   * @param  {Number} [props.handheldCols=4] number of columns to show on small screens
   * @param  {Number} [props.tabletCols=8] number of columns to show on medium screens
   * @param  {Number} [props.desktopCols=12] number of columns to show on usual screens
   * @param  {Number} [props.fullCols=12] number of columns to show on large screens
   * @param  {Number} [props.gutter=10] pixel width of gutters
   * @param  {Number} [props.baseSize=80] base size of one column. This value will be used
   *                                      to determine the breakpoints
   * @param  {String} [props.el='div'] element to render as Grid.
   * @return {ReactElement} returns a DOMnode with the grid logic
   */
  constructor (props) {
    super(props)
    this.state = {
      columns: this.props.tabletCols
    }
    this.handleHandheld = this.handleHandheld.bind(this)
    this.handleTablet = this.handleTablet.bind(this)
    this.handleDesktop = this.handleDesktop.bind(this)
    this.handleFull = this.handleFull.bind(this)
  }

  getChildContext () {
    return {
      columns: this.state.columns,
      gutter: this.context.gutter || this.props.gutter,
      handheldCols: this.context.handheldCols || this.props.handheldCols,
      tabletCols: this.context.tabletCols || this.props.tabletCols,
      desktopCols: this.context.desktopCols || this.props.desktopCols,
      baseSize: this.context.baseSize || this.props.baseSize
    }
  }

  /**
   * before the component mounts we need to add media listeners.
   * each breakpoint exactly matches a certain width. There are no overlaps
   * of breakpoints.
   *
   * When the listeners are created we need to call them once to get the initial layout.
   * This should happen before we render the component.
   * @private
   */
  componentWillMount () {
    const {baseSize, handheldCols, tabletCols, desktopCols} = this.props
    const matchers = getMatchers(baseSize, handheldCols, tabletCols, desktopCols)

    this.handheld = matchers.handheld
    this.tablet = matchers.tablet
    this.desktop = matchers.desktop
    this.full = matchers.full

    this.handheld.addListener(this.handleHandheld)
    this.tablet.addListener(this.handleTablet)
    this.desktop.addListener(this.handleDesktop)
    this.full.addListener(this.handleFull)

    this.handleHandheld(this.handheld)
    this.handleTablet(this.tablet)
    this.handleDesktop(this.desktop)
    this.handleFull(this.full)
  }

 /**
   * before the component is unmounted we need to make sure all
   * listeners are removed properly.
   * @private
   */
  componentWillUnmount () {
    this.handheld.removeListener(this.handleHandheld)
    this.tablet.removeListener(this.handleTablet)
    this.desktop.removeListener(this.handleDesktop)
    this.full.removeListener(this.handleFull)
  }

  /**
   * handle the handheld breakpoint
   * @private
   * @param  {Object} mql mediaQueryListener for handheld
   */
  handleHandheld (mql) {
    if (mql.matches) {
      this.setState({
        columns: this.props.handheldCols
      })
    }
  }

  /**
   * handle the tablet breakpoint
   * @private
   * @param  {Object} mql mediaQueryListener for tablet
   */
  handleTablet (mql) {
    if (mql.matches) {
      this.setState({
        columns: this.props.tabletCols
      })
    }
  }

  /**
   * handle the desktop breakpoint
   * @private
   * @param  {Object} mql mediaQueryListener for desktop
   */
  handleDesktop (mql) {
    if (mql.matches) {
      this.setState({
        columns: this.props.desktopCols
      })
    }
  }

  /**
   * handle the full-width breakpoint
   * @private
   * @param  {Object} mql mediaQueryListener for full-width
   */
  handleFull (mql) {
    if (mql.matches) {
      this.setState({
        columns: this.props.fullCols
      })
    }
  }

  render () {
    const classes = classNames(this.props.className, styles.schachtelGrid)
    return (
      <this.props.el className={classes}>
        {this.props.children}
      </this.props.el>
    )
  }
}

Grid.defaultProps = {
  el: 'div',
  handheldCols: 4,
  tabletCols: 8,
  desktopCols: 12,
  fullCols: 12,
  gutter: 10,
  baseSize: 80
}

Grid.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  el: PropTypes.string,
  gutter: PropTypes.number,
  handheldCols: PropTypes.number,
  tabletCols: PropTypes.number,
  desktopCols: PropTypes.number,
  fullCols: PropTypes.number,
  baseSize: PropTypes.number
}

Grid.childContextTypes = {
  columns: PropTypes.number,
  gutter: PropTypes.number,
  handheldCols: PropTypes.number,
  tabletCols: PropTypes.number,
  desktopCols: PropTypes.number,
  fullCols: PropTypes.number,
  baseSize: PropTypes.number
}

Grid.contextTypes = {
  columns: PropTypes.number,
  gutter: PropTypes.number
}

export default Grid
