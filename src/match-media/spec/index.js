/* eslint-env jest */
import matchMedia from '..'
import '../../match-media-mock'

describe('matchMedia', () => {
  it('returns an object', () => {
    const matcher = matchMedia({})
    expect(typeof matcher).toBe('object')
  })
  it('handles minWidth', () => {
    const matcher = matchMedia({
      minWidth: '100px'
    })
    expect(matcher.media).toEqual('(min-width: 100px)')
  })
  it('handles maxWidth', () => {
    const matcher = matchMedia({
      maxWidth: '800px'
    })
    expect(matcher.media).toEqual('(max-width: 800px)')
  })
  it('handles minWidth and maxWidth', () => {
    const matcher = matchMedia({
      minWidth: '100px',
      maxWidth: '800px'
    })
    expect(matcher.media).toEqual('(min-width: 100px) and (max-width: 800px)')
  })
})
