/**
 * wrapper around matchMedia to use objects instead of a string
 * @param  {Object} mq mediaQuery object
 * @param  {String} [mq.minWidth] minimum width for query
 * @param  {String} [mq.maxWidth] maximum width for query
 * @return {matchMedia} returns the native `window.matchMedia`
 */
const matchMedia = mq => {
  const {minWidth, maxWidth} = mq
  const mqArgs = []
  if (minWidth) {
    mqArgs.push(`(min-width: ${minWidth})`)
  }
  if (maxWidth) {
    mqArgs.push(`(max-width: ${maxWidth})`)
  }
  return window.matchMedia(mqArgs.join(' and '))
}

export default matchMedia
