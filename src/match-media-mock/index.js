global.window.matchMedia = (media) => {
  return {
    addListener (cb) {
      cb({
        matches: true,
        media
      })
    },
    removeListener () {},
    matches: false,
    media
  }
}
