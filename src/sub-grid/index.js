import React, {PropTypes} from 'react'
import classNames from 'classnames'

import styles from './style.css'

/**
 * A subgrid simply takes kae of the gutters
 * and opens a new `flexbox` wrapper
 * @param  {Object} [props] options
 * @param  {String} [props.el='div'] element to render as subgrid.
 * @param  {String} [props.className] additional classNames to add to the subgrid.
 * @param  {String} [props.children] children to render (usually the nested content).
 * @return {ReactElement} returns a DOMnode with the subgrid logic
 */
const SubGrid = (props, context) => {
  const style = {
    margin: `0 ${context.gutter * -1}px`
  }
  const classes = classNames(props.className, styles.schachtelSubGrid)
  return (
    <props.el style={style} className={classes}>
      {props.children}
    </props.el>
  )
}

SubGrid.defaultProps = {
  el: 'div'
}

SubGrid.contextTypes = {
  gutter: PropTypes.number
}

SubGrid.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  el: PropTypes.string
}

export default SubGrid
