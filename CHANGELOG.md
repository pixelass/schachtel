# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.2.0"></a>
# [0.2.0](https://github.com/pixelass/schachtel/compare/v0.1.0...v0.2.0) (2016-10-05)


### Features

* **columns:** hide columns ([d46eed0](https://github.com/pixelass/schachtel/commit/d46eed0))



<a name="0.1.0"></a>
# [0.1.0](https://github.com/pixelass/schachtel/compare/v0.0.1...v0.1.0) (2016-09-27)


### Features

* **overlay:** debugging layer ([79e2f4c](https://github.com/pixelass/schachtel/commit/79e2f4c))



<a name="0.0.1"></a>
## 0.0.1 (2016-09-27)


### Bug Fixes

* **test:** dependencies ([cafddc7](https://github.com/pixelass/schachtel/commit/cafddc7))


### Features

* **basics:** added basic elements ([d90c322](https://github.com/pixelass/schachtel/commit/d90c322))
