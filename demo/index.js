import React from 'react'
import ReactDOM, {render} from 'react-dom' // eslint-disable-line no-unused-vars
import classNames from 'classnames'
import SyntaxHighlighter from 'react-syntax-highlighter'
import { github } from 'react-syntax-highlighter/dist/styles'

import {Grid, SubGrid, Column} from '../'
import GridOverlay from '../lib/grid-overlay'
const app = document.getElementById('app')
import styles from './style.css'


const codeExamples = [
  `<Column handheld={4} tablet={6}>
  <SubGrid>SubGrids have no gutter </SubGrid>
  <SubGrid>I am a subgrid with 4 columns. </SubGrid>
  <SubGrid>On tablets and above I have 6 columns. </SubGrid>
  <SubGrid>
    <Column handheld={6}>
      I am requesting 6 columns but will only get 4
      until more are available.
    </Column>
    <Column handheld={12}>
      If my parents allowed it, I would be 12 units wide.
    </Column>
  </SubGrid>
</Column>`,
  `<Column handheld={4}
           tablet={6}>
  <SubGrid>
    <Column handheld={2}
             tablet={4}>
      2 -> 4
    </Column>
    <Column handheld={2}>
      2
    </Column>
    <Column handheld={4}
             tablet={2}>
      4 -> 2
    </Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
    <Column>1</Column>
  </SubGrid>
</Column>`
]

const App = () => (
  <div>
    <GridOverlay visible/>
    <Grid className={styles.debug} el='header'>
      <Column className={classNames(styles.logo, styles.debug)} el='h1' handheld={4} tablet={3}>schachtel</Column>
      <Column className={classNames(styles.slogan, styles.debug)} handheld={5} desktop={9} el='h2'>The shallow grid with infinite depth.</Column>
    </Grid>
    <Grid el='main'>
      <Column className={styles.debug} el='h1' handheld={12}>Why "schachtel"?</Column>
      <Column className={styles.debug} handheld={8}>
        Schachtel is a shallow grid, that does not need
        extra elements just make that layout work.
        <br/>
        Schachtel does not care about the nesting.
        If you define a <code>Column</code> to be <code>2</code> units wide, it will always be <code>2</code> units wide.
        At different breakpoints the number of columns change (usually: 4-8-12).
        <br/>
        <h4>This mechanism is based on the oldschool 960-grid logic.</h4>
        1 col = 80px.
        <br/>
        12 col = 960px.
        <br/>
        <h4>Therefore</h4>
        4 col = 320px. (0 - 480px)
        <br/>
        8 col = 640px. (481px - 800px)
        <br/>
        12 col = 960px. (801px - 1200px)
        <br/>
        <p>
          This grid uses <code>flexbox</code> and is fully fluid/responsive.
          <br/>
          Everything is adjustable but don't expect the usual grid behaviour.
          <br/>
          SubGrids open a new grid, which has as many columns as its parents unit-size.
        </p>
        <h5>So instead of defining something like this</h5>
        <pre>
          .col-6 // expect 50% of parent = 6{'\n'}
          {'  '}.col-6 // expect 50% of parent = 3{'\n'}
          {'  '}.col-6 // expect 50% of parent = 3{'\n'}
        </pre>
        <h5>You should expect this model</h5>
        <pre>
          .col-6 // = 6{'\n'}
          {'  '}.col-3 // = 3{'\n'}
          {'  '}.col-3 // = 3{'\n'}
        </pre>
        <h4>Example is rendered below</h4>
        <SyntaxHighlighter language='xml' style={github}>{codeExamples[0]}</SyntaxHighlighter>
        <SubGrid>
          <Column className={styles.debug} handheld={4} tablet={6}>
            <SubGrid>SubGrids have no gutter </SubGrid>
            <SubGrid>I am a subgrid with 4 columns. </SubGrid>
            <SubGrid>On tablets and above I have 6 columns. </SubGrid>
            <SubGrid>
              <Column className={styles.debug} handheld={6}>
                I am requesting 6 columns but will only get 4
                until more are available.
              </Column>
              <Column className={styles.debug} handheld={12}>
                If my parents allowed it, I would be 12 units wide.
              </Column>
            </SubGrid>
          </Column>
        </SubGrid>
        <SyntaxHighlighter language='xml' style={github}>{codeExamples[1]}</SyntaxHighlighter>
        <SubGrid>
          <Column className={styles.debug} handheld={4}
           tablet={6}>
            <SubGrid>
              <Column className={styles.debug} handheld={2}
                       tablet={4}>
                2 -> 4
              </Column>
              <Column className={styles.debug} handheld={2}>
                2
              </Column>
              <Column className={styles.debug} handheld={4}
                       tablet={2}>
                4 -> 2
              </Column>
              <Column className={styles.debug}>1</Column>
              <Column className={styles.debug}>1</Column>
              <Column className={styles.debug}>1</Column>
              <Column className={styles.debug}>1</Column>
            </SubGrid>
          </Column>
        </SubGrid>
        <h3>Hide Columns</h3>
        <SubGrid>
        <Column tablet={0} className={styles.debug}>Col 1 (1 -> 0)</Column>
        <Column tablet={2} className={styles.debug}>Col 2 (1 -> 2)</Column>
        <Column tablet={0} className={styles.debug}>Col 3 (1 -> 0)</Column>
        <Column tablet={2} className={styles.debug}>Col 4 (1 -> 2)</Column>
        <Column tablet={0} className={styles.debug}>Col 5 (1 -> 0)</Column>
        <Column tablet={2} className={styles.debug}>Col 6 (1 -> 2)</Column>
        <Column tablet={0} className={styles.debug}>Col 7 (1 -> 0)</Column>
        <Column tablet={2} className={styles.debug}>Col 8 (1 -> 2)</Column>
        </SubGrid>
      </Column>
      <Column className={styles.debug} handheld={8} desktop={4} el='aside'>
        <h3>Sidebar</h3>
      </Column>
    </Grid>
    <Grid el='footer'>
      <Column className={styles.debug} handheld={12} el='h3'>
        Footer
      </Column>
    </Grid>
  </div>
)

render(<App/>, app)
